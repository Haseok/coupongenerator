# Coupon Generator / Spring Boot / React

### Intro

사용자로부터 이메일 주소를 입력으로 받아서 16자리의 알파벳과 숫자로 이루어진 ***중복없는*** 쿠폰 번호를 발급하고 발급된 쿠폰 정보를 같은 페이지에 리스팅 하는 웹어플리케이션 입니다.  
- [Live Demo](http://13.125.72.211:3000/)


### Server Side: Spring Boot

스프링 부트는 RESTful service 를 쉽고 빠르게 생성할 수 있습니다. /api/coupon/v1 을 GET Method 호출하면 CouponGeneratorController 내의 Get List Service 가 수행되고 발행된 쿠폰의 리스트가 반환 됩니다. 
/api/coupon/v1 을 POST Method 호출하면 위의 컨트롤러에서 Coupon Generator Service 가 수행되고 중복 및 기타 예외 처리후 데이터가 저장 됩니다. 그리고 In-memory DB 인 H2 데이터베이스에 더미데이터가 1,000건 입력되어 있습니다. 
JSON Object 통신으로 클라이언트와 통신을 하도록 구현 되어 있습니다. Junit을 이용한 단위테스트 WebMVC 테스트를 확인 할 수 있습니다.


### Client Side: ReactJS

React 로 구현 된 클라이언트는 서버와 JSON Object 통신을 하여 데이터를 주고 받습니다. 이메일을 입력하면 쿠폰 번호를 발행하고, 쿠폰 번호 테이블은 업데이트 됩니다. 단, 중복된 이메일 입력에 따른 쿠폰 발행은 안됩니다.
SPA로 구현이 되어 있고, bootstrap, reactstrap, react-js-pagenation 을 참조 합니다. 현재 구현된 5개의 컴포넌트가 있으며, App.js 를 기준으로 수많은 컴포넌트를 각각 참조하여 빌드 됩니다. 
사용자 입력 후 서버 반환값 업데이트는 fetch 로 구현이 되어 있어서 페이지 이동이 없습니다.


### Solution 
- 쿠폰 번호 알고리즘 (RandomGeneratorUtil.class)
   - 객체 생성 시 Contstruct에서 [0-9a-zA-Z]로 temporary 배열에 각각 1자씩 입력되어 객체가 생성 됩니다. 
   - generate() Method 호출 시 앞에서 생성된 객체의 temporary 배열을 이용하여 response 배열에 랜덤한 문자를 할당 합니다.

- 쿠폰 발행 과정
   - 사용자로부터 이메일을 입력 받고, Solution 의 generate() 를 이용하여 쿠폰을 매핑 합니다.
   - 정상 등록이 되었으면 해당 내용을 클라이언트에 반환 합니다.
   - 단, 이메일의 중복 검사 및 쿠폰 중복으로 인한 입력 불가 시 클라이언트에 알려 줍니다.


### Setup

- Install node version 8.x, 9.X
- Install npm version 5.6.0
- Install Java jdk 1.8
- Install Maven 3.x, 4.x
- Install yarn 1.5.6
- JDK, Maven, Node 등 필요에 맞는 환경 변수를 설정 합니다.


### Install & build

- [back-end] 폴더에서 run "mvn install" 을 실행 합니다.
- [back-end/target] 폴더에서 run "java -jar CouponGeneratorRestAPI-1.0.jar" 을 실행 합니다.
- [front-end] 폴더에서 yarn start


### Usage

- [URL access]
   - http://localhost:3000 으로 접속하여 확일할 수 있습니다.
- [API access]
   - http://localhost:8080/v1/coupon : method[GET] - select list, mothod[POST] - insert
- [DB access]
   - http://localhost:8080/console
   - id: kakao, pw: pay


### References
[1] https://docs.spring.io 
[2] https://developer.mozilla.org 
