package com.coupon.generator.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coupon.generator.service.CouponService;
import com.coupon.generator.util.RandomGeneratorUtil;

@RestController
@RequestMapping("/v1/coupon")
public class ApiCouponController {

	@Autowired
	CouponService couponService;
	
	@Autowired
	RandomGeneratorUtil randomGeneratorUtil;

	@CrossOrigin
	@GetMapping("")
	public Map<String, Object> list(Pageable pageable) {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", true);
		map.put("data", couponService.getCouponList(pageable));
		return map;
	}

	@CrossOrigin
	@PostMapping("")
	public Map<String, Object> insert(@RequestBody Map<String, Object> body) {

		String eamil = (String) body.get("email");
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("result", true);
		map.put("data", couponService.insertCoupon(eamil));

		return map;
	}
	
}
