package com.coupon.generator.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice(annotations = RestController.class)
public class ExceptionControllerAdvice {
	@ExceptionHandler(Exception.class)
	public ResponseEntity<String> getSQLError(Exception exception) {
		HttpHeaders headers = new HttpHeaders();
		String message = "{\"error\":\"can not found your order.\"}";
		headers.set("HeaderKey", "HeaderDetails");
		return new ResponseEntity<String>(message, headers, HttpStatus.ACCEPTED);
	}
}