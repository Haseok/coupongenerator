package com.coupon.generator.factory;

import org.springframework.stereotype.Component;

import com.coupon.generator.model.Coupon;
import com.coupon.generator.util.RandomGeneratorUtil;

@Component
public class CouponFactory {

	public Coupon create(String email) {
		RandomGeneratorUtil randomGenerator = new RandomGeneratorUtil();
		return new Coupon(email, randomGenerator.generate());
	}
	
}
