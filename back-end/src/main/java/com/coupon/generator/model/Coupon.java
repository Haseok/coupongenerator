package com.coupon.generator.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Coupon {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonProperty("cid")
	protected Long id;

	@Column(nullable = false, unique = true, length = 64)
	@JsonProperty("email")
	protected String email;

	@Column(nullable = false, unique = true, length = 16)
	@JsonProperty("coupon")
	protected String coupon;

	@CreatedDate
	@JsonIgnore
	protected LocalDateTime createDate;

	public Coupon() {

	}

	public Coupon(String email, String coupon) {
		this.email = email;
		this.coupon = coupon;
		this.createDate = LocalDateTime.now();
	}

	public Coupon(Long id, String email, String coupon, LocalDateTime createDate) {
		this.id = id;
		this.email = email;
		this.coupon = coupon;
		this.createDate = (createDate == null) ? LocalDateTime.now() : createDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCoupon() {
		return coupon;
	}

	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	@JsonProperty("create_date")
	public String getFormattedCreateDate() {
		return getFormattedDate(createDate, "yyyy.MM.dd HH:mm:ss");
	}

	public String getFormattedDate(LocalDateTime dateTime, String format) {
		if (dateTime == null) {
			return "";
		}
		return dateTime.format(DateTimeFormatter.ofPattern(format));
	}

	@Override
	public String toString() {
		return "Coupon [id=" + id + ", email=" + email + ", coupon=" + coupon + ", createDate=" + createDate + "]";
	}

}
