package com.coupon.generator.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.coupon.generator.model.Coupon;

public interface CouponRepository extends JpaRepository<Coupon, Long>{

	long countByEmail(String email);
	
	boolean existsByEmail(String email);

}
