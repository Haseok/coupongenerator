package com.coupon.generator.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.coupon.generator.model.Coupon;

public interface CouponService {

	public Page<Coupon> getCouponList(Pageable pageable);

	public Coupon insertCoupon(String email);

}
