package com.coupon.generator.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.coupon.generator.factory.CouponFactory;
import com.coupon.generator.model.Coupon;
import com.coupon.generator.repository.CouponRepository;


@Service("couponService")
public class CouponServiceImpl implements CouponService {

	@Autowired
	private CouponRepository couponRepository;

	@Autowired
	private CouponFactory couponFactory;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public Page<Coupon> getCouponList(Pageable pageable) {
		return couponRepository.findAll(pageable);
	}

	@Override
	public Coupon insertCoupon(String email) {

		if (!StringUtils.hasText(email)) {
			return null;
		}

		if (couponRepository.existsByEmail(email)) {
			return null;
		}

		
		try {
			Coupon coupon = couponFactory.create(email);
			System.out.println(coupon);
			return couponRepository.save(coupon);
		} catch (Exception e) {
			// already exist (email or coupon)
			logger.error(e.getMessage());
			return null;
		}
	}

}
