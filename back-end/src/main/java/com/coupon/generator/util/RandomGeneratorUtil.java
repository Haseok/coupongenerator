package com.coupon.generator.util;

import java.security.SecureRandom;
import java.util.Objects;
import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class RandomGeneratorUtil {

	public static final String RANDOM_DATA = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private final Random random;
	private final char[] temBuf;
	private final char[] resBuf;


	public RandomGeneratorUtil() {
		
		this(16, new SecureRandom(), RANDOM_DATA);
	}
	
	public RandomGeneratorUtil(int length, Random random, String data) {
		
		this.random = Objects.requireNonNull(random);
		this.temBuf = data.toCharArray();
		this.resBuf = new char[length];
	}

	public String generate() {

		for (int idx = 0; idx < resBuf.length; ++idx) {
			resBuf[idx] = temBuf[random.nextInt(temBuf.length)];
		}

		return new String(resBuf);
	}

}