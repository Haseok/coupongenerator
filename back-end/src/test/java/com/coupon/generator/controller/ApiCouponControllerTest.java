package com.coupon.generator.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.coupon.generator.model.Coupon;
import com.coupon.generator.service.CouponService;
import com.coupon.generator.util.RandomGeneratorUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
@EnableSpringDataWebSupport
@AutoConfigureMockMvc
public class ApiCouponControllerTest {
	
	private static String email1 = "aaa@abc.com";
	private static String email2 = "bbb@abc.com";
	private static String email3 = "ccc@abc.com";
	private static Coupon coupon1;
	private static Coupon coupon2;
	private static Coupon coupon3;
	private static LocalDateTime now = LocalDateTime.now();

	@InjectMocks
	private ApiCouponController apiCouponController;
	
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private CouponService couponService;

	@MockBean
	private RandomGeneratorUtil randomGeneratorUtil;

	@BeforeClass
	public static void init() {
		
		coupon1 = new Coupon(1000L, email1, "abcdabcdabcdabcd", now);
		coupon2 = new Coupon(1001L, email2, "bbbbbbbbbbbbbbbb", now);
		coupon3 = new Coupon(1002L, email3, "1234567891234567", now);
	}
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCouponList_WebMvcConnection() throws Exception {
		
		/**
		 * Mock Object Pagenation!
		 */
		
		MockHttpServletResponse res = this.mockMvc.perform(get("/v1/coupon").param("page", "0").param("size", "20"))
				//.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=utf-8"))
				.andExpect(jsonPath("$.result", is(true)))
				.andReturn().getResponse();
		System.out.println(res.getContentAsString());
	}
	
	@Test
	public void testCouponList_CouponServiceBehavior() {
		couponService.getCouponList(null);
		verify(couponService).getCouponList(null);
	}

	@Test
	public void testCouponInsert_WebMvcConnection() throws Exception {

		when(couponService.insertCoupon(email1)).thenReturn(coupon1);
		
		String json = "{\"email\":\"aaa@abc.com\"}";
		
		this.mockMvc.perform(post("/v1/coupon").contentType(MediaType.APPLICATION_JSON).content(json))
				//.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=utf-8"))
				.andExpect(jsonPath("$.result", is(true)))
				.andReturn();
		
		verify(couponService, times(1)).insertCoupon(email1);
	}
	
	@Test
	public void testInsertCoupon_CouponServiceBehavior() {
		String email = "aaa@abc.com";
		couponService.insertCoupon(email);
		verify(couponService).insertCoupon(email);
	}
}
