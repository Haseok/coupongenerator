package com.coupon.generator.factory;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.coupon.generator.model.Coupon;
import com.coupon.generator.util.RandomGeneratorUtil;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {CouponFactory.class, Coupon.class, RandomGeneratorUtil.class})
public class CouponFactoryTest {
	
	private static String email = "aaa@abc.com";

	@InjectMocks
	private CouponFactory couponFactory;
	
	@MockBean
	private RandomGeneratorUtil randomGenerator;
	
	@BeforeClass
	public static void init() {		
	}
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testRandomGenerator_RandomGeneratorBehavior() {
		randomGenerator.generate();
		verify(randomGenerator).generate();
	}

	@Test
	public final void testCreate() {
		assertNotNull(couponFactory.create(email));
	}

}
