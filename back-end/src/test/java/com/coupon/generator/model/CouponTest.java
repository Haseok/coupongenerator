package com.coupon.generator.model;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { Coupon.class })
public class CouponTest {

	private Coupon coupon;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCoupon_TwoParamsConstructor() {

		coupon = new Coupon("abc@abc.com", "aaaabbbbccccdddd");
		assertEquals(coupon.email, coupon.getEmail());
		assertEquals(coupon.coupon, coupon.getCoupon());
		assertEquals(coupon.createDate, coupon.getCreateDate());
	}

	@Test
	public void testCoupon_FourParamsConstructor() {

		LocalDateTime now = LocalDateTime.now();

		coupon = new Coupon(1000L, "abc@abc.com", "aaaabbbbccccdddd", now);
		assertEquals(coupon.id, coupon.getId());
		assertEquals(coupon.email, coupon.getEmail());
		assertEquals(coupon.coupon, coupon.getCoupon());
		assertEquals(coupon.createDate, coupon.getCreateDate());
	}
}
