package com.coupon.generator.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.coupon.generator.factory.CouponFactory;
import com.coupon.generator.model.Coupon;
import com.coupon.generator.repository.CouponRepository;


@RunWith(SpringRunner.class)
@SpringBootTest
public class CouponServiceImplTest {

	public static final Long CID = 1000L;
	public static final String EMAIL = "aaa@bbb.com";
	public static final String COUPON = "aaaabbbbccccdddd";

	@InjectMocks
	CouponServiceImpl couponService;

	@Mock
	CouponRepository couponRepository;

	@Mock
	CouponFactory couponFactory;

	@Mock
	Logger logger;

	Coupon coupon;
	LocalDateTime now;

	@Before
	public void setUp() throws Exception {
		now = LocalDateTime.now();
		coupon = new Coupon();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testGetCouponList_EmptyParams() {
		assertNull(couponService.getCouponList(null));
	}

	@Test
	public final void testInsertCoupon_EmptyParams() {
		assertNull(couponService.insertCoupon(null));
		verifyZeroInteractions(couponRepository, couponFactory);
	}

	@Test
	public final void testInsertCoupon_ExistsByEmailTrue() {
		when(couponRepository.existsByEmail(EMAIL)).thenReturn(true);
		assertNull(couponService.insertCoupon(EMAIL));
		verifyZeroInteractions(couponFactory);
	}

	@Test
	public final void testInsertCoupon_SaveException() {
		coupon = new Coupon(CID, EMAIL, COUPON, now);
		when(couponRepository.save(coupon)).thenThrow(new RuntimeException());
		assertNull(couponService.insertCoupon(EMAIL));
	}

	@Test
	public final void testInsertCoupon_Success() {
		coupon = new Coupon(CID, EMAIL, COUPON, now);
		when(couponRepository.existsByEmail(EMAIL)).thenReturn(false);
		when(couponRepository.save(coupon)).thenReturn(coupon);
		when(couponFactory.create(EMAIL)).thenReturn(coupon);
		assertEquals(coupon, couponService.insertCoupon(EMAIL));
	}

}
