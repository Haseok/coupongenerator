package com.coupon.generator.util;

import static org.junit.Assert.assertNull;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { LocalDateTimeConverterUtil.class })
public class LocalDateTimeConverterUtilTest {

	private LocalDateTimeConverterUtil localDateTimeConverterUtil;
	LocalDateTime localDateTime;
	Timestamp timestamp;
	
	@Before
	public void setUp() throws Exception {
		localDateTimeConverterUtil = new LocalDateTimeConverterUtil();
		localDateTime = LocalDateTime.now();
		timestamp = new Timestamp(System.currentTimeMillis());
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testConvertToDatabaseColumn_EmptyParams() {
		assertNull(localDateTimeConverterUtil.convertToDatabaseColumn(null));
	}

	@Test
	public final void testConvertToDatabaseColumn() {
		System.out.println(localDateTimeConverterUtil.convertToDatabaseColumn(localDateTime));
	}

	@Test
	public final void testConvertToEntityAttribute_EmptyParams() {
		assertNull(localDateTimeConverterUtil.convertToEntityAttribute(null));
	}

	@Test
	public final void testConvertToEntityAttribute() {
		System.out.println(localDateTimeConverterUtil.convertToEntityAttribute(timestamp));
	}

}
