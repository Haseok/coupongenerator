package com.coupon.generator.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { RandomGeneratorUtil.class })
public class RandomGeneratorUtilTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testRandomGeneratorUtil_EmptyRandomGeneratorUtilConstructor() {

		RandomGeneratorUtil randomGeneratorUtil = new RandomGeneratorUtil();
		assertEquals(randomGeneratorUtil.generate().length(), 16);
	}

	@Test
	public final void testRandomGeneratorUtil_NonEmptyRandomGeneratorUtilConstructor() {

		Random random = new SecureRandom();
		String data = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

		RandomGeneratorUtil randomGeneratorUtil = new RandomGeneratorUtil(16, random, data);
		assertEquals(randomGeneratorUtil.generate().length(), 16);
	}
	
	@Test
	public final void testRandomGeneratorUtil_NotEquals() {

		int cnt = 0;
		RandomGeneratorUtil randomGeneratorUtil = new RandomGeneratorUtil();
		
		/**
		 * 10000K - 32s
		 */
		while (cnt++ < 100) {
			assertNotEquals(randomGeneratorUtil.generate(), randomGeneratorUtil.generate());
		}
	}

	@Test
	@Ignore
	public final void testRandomPrint() {

		int cnt = 0;
		int max = 1000;
		Random random = new SecureRandom();
		String data = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		RandomGeneratorUtil coupon = new RandomGeneratorUtil();
		RandomGeneratorUtil email = new RandomGeneratorUtil(8, random, data);
		LocalDateTime now = LocalDateTime.now();

		File file = new File("dummy.sql");

		try {
			PrintStream printStream = new PrintStream(new FileOutputStream(file));

			System.setOut(printStream);

			while (cnt++ < max) {

				System.out.print("INSERT INTO coupon (id, coupon, create_date, email) VALUES (");
				System.out.print(cnt + ", ");
				System.out.print("'" + coupon.generate() + "', ");
				System.out.print("'" + now.minusMinutes(max - cnt) + "', ");
				System.out.println("'" + email.generate() + "@google.com" + "')");

			}

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
