import React, { Component } from 'react';
import './css/common.css';
import { Container } from 'reactstrap';
import HeaderComponent from './components/HeaderComponent';
import ListComponent from './components/ListComponent';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Container>
            <HeaderComponent/>
            <ListComponent />
        </Container>
      </div>
    );
  }
}

export default App;
