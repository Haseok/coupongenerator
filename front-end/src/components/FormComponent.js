import React, {Component} from 'react'
import { Form, FormGroup, Input, Button } from 'reactstrap';
import ToastrContainer, {Toast, ToastDanger} from 'react-toastr-basic';

class FormComponent extends Component{

    constructor(props) {
        super(props);

        this.state = {email:''};

        this._handleChange = this._handleChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);
    }


    _handleChange = (e) => {
        this.setState({email: e.target.value});
    }


    _handleSubmit = (e) => {
        e.preventDefault();

        fetch('http://localhost:8080/v1/coupon', {
            method: 'POST',
            body: JSON.stringify(this.state),
            headers: {
                "Content-Type": "application/json"
            }
        }).then(res => res.json())
          .then(res => {

              if (res.result) {
                  if(res.data == null){
                      Toast('쿠폰 발급 불가 : 이미 발행된 이메일');
                  }else{
                      this.props._getCouponList()
                      this.setState({email:''})
                      Toast('등록 되었습니다.');
                  }
              }else{
                  ToastDanger('서버 오류가 발생 하였습니다. 잠시후 다시 시도해 주세요.');
              }
          })
          .catch(err => console.log(err))
    }


    render () {
        return (
            <Form onSubmit={this._handleSubmit}>
                <ToastrContainer />
                <FormGroup>
                    <h6 className="mb-1">이메일을 입력 해주세요.</h6>
                    <Input type="email"
                            name="email"
                            value={this.state.email}
                            onChange={this._handleChange}
                            placeholder="your@email.com"
                            required/>
                    {/* <Input invalid /> */}
                </FormGroup>
                <Button color="secondary" size="sm" block>쿠폰 발행</Button>
            </Form>
        )
    }
}

export default FormComponent
