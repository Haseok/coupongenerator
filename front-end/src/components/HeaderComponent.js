import React, {Component} from 'react'
import { Collapse, Badge, Row, Col } from 'reactstrap';

class Header extends Component{

    constructor(props) {
        super(props);

        this.state = { collapse: false };
        this._toggle = this._toggle.bind(this);
    }


    _toggle() {
        this.setState({ collapse: !this.state.collapse });
    }


    render () {
        return (
            <div className="mt-5">
                <Row><Col>
                	<style>
                		{
        		          `.custom-tag {
                        		font-size:0.2rem;
                        		margin-left:5px;
                        		cursor:pointer;
            		       }`
                		}
                	</style>

            		<h3 className="alert-heading inline">
            			[Coupon Generator]
            			<Badge className="custom-tag" color="primary" onClick={this._toggle}>더보기</Badge>
            		</h3>
            		<Collapse isOpen={this.state.collapse}>
            			<h5 className="mt-3">
            				사용자로부터 이메일 주소를 입력으로 받아서 16자리의 알파벳과 숫자로 이루어진 ***중복없는*** 쿠폰 번호를 발급하고, 발급된 쿠폰 정보를 같은 페이지에 리스팅 하는 웹어플리케이션 입니다.
            			</h5>
            			<h6 className="mb-0">
            			사용 방법 : 아래의 입력창에 이메일을 입력하면, 하단 리스트에 추가 됩니다.
                        </h6>
            		</Collapse>
                </Col></Row>
            </div>
        )
    }
}

export default Header;
