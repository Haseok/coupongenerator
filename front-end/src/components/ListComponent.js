import React, {Component} from 'react'
import Pagination from "react-js-pagination";
import PropTypes from 'prop-types';
import { Table, Label, Row, Col } from 'reactstrap';
import ListTable from './ListTableComponent';
import Spinner from './Spinner';
import FormComponent from './FormComponent';

class ListComponent extends Component{

    constructor(props) {
        super(props);

        this.state = {
            activePage: 1,
            totalPages:0,
            totalElements:0,
            page:1,
            size:20,
            first:true,
            last:false,
            coupons: []
        };

        this._getCouponList = this._getCouponList.bind(this);
        this._callApi = this._callApi.bind(this);
        this._setApi = this._setApi.bind(this);
        this._renderTableList = this._renderTableList.bind(this);
        this._renderPagenation = this._renderPagenation.bind(this);
        this._dashRegularExpression = this._dashRegularExpression.bind(this);

    }


    componentWillMount(){
        this._getCouponList()
    }


    componentDidMount(){
    }


    _getCouponList = async () => {

        const coupons = await this._callApi(1);

        if( coupons !== undefined ){

            this.setState({
                activePage: 1,
                coupons:coupons
            })

            this._renderPagenation()
        }
    }


    _callApi = (page) => {

        page  = page < 1 ? 0 : page - 1;

        let url = 'http://localhost:8080/v1/coupon?page=' + page + '&size=' + this.state.size + '&sort=id,desc';

        return fetch(url)
            .then((res) => res.json())
            .then((json) => this._setApi(json.data))
            .catch(err => console.log(err))
    }


    _setApi = (data) => {

        this.setState({
            activePage: data.number+1,
            totalPages:data.totalPages,
            totalElements:data.totalElements,
            page:data.number,
            size:data.size,
            first:data.first,
            last:data.last,
            coupons:data.content
        });

        return data.content;
    }


    _renderTableList = (coupons) => {

        const res = coupons.map((item) => {

            return (
                <ListTable
                    key={item.cid}
                    cid={item.cid}
                    email={item.email}
                    coupon={this._dashRegularExpression(item.coupon)}
                    datetime={item.create_date}
                    />
            )
        });

        return res;
    }


    _renderPagenation = () => {

        return (

            <Pagination
                pageRangeDisplayed={10}
                activePage={this.state.activePage}
                itemsCountPerPage={this.state.size}
                totalItemsCount={this.state.totalElements}
                onChange={this._callApi}
                />
        )
    }

    _dashRegularExpression = (code) =>{

        let len, x, res;

        len = code.length;
        x = len % 4 ;
        res = code.substring(0, x);

        while (x < len) {
           if (res !== "") res += "-";
           res += code.substring(x, x + 4);
           x += 4;
        }

        return res;
    }

    render () {

        const coupons = this.state.coupons;
        const spiner = (
            <Spinner />
        );

        return (
            <div className="mt-5">

                <Row>
                    <Col><FormComponent _getCouponList={this._getCouponList} /></Col>
                </Row>

                <hr/>

                <Row>
                    <Col>
                        <Label>리스트</Label>
                        <Table striped size="sm">
                        	<thead>
                        		<tr>
                        			<th>id</th>
                        			<th>Email</th>
                        			<th>Coupon</th>
                        			<th>Datetime</th>
                        		</tr>
                        	</thead>
                        	<tbody>
                                {coupons.length ? this._renderTableList(coupons) : <tr><td>spiner</td></tr>}
                        	</tbody>
                        </Table>
                        <div>
                        	<div className='text-center'>
                                {coupons.length ? this._renderPagenation() : spiner}
                        	</div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

Table.propTypes = {
    // Pass in a Component to override default element
    tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
    size: PropTypes.string,
    bordered: PropTypes.bool,
    striped: PropTypes.bool,
    dark: PropTypes.bool,
    hover: PropTypes.bool,
    responsive: PropTypes.bool
};

export default ListComponent;
