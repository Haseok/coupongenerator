import React from 'react';
import PropTypes from 'prop-types';

function ListTable({cid, email, coupon, datetime}){

    return (
        <tr>
            <td>{cid}</td>
            <td>{email}</td>
            <td>{coupon}</td>
            <td>{datetime}</td>
        </tr>
    )
}

ListTable.propTypes = {
    cid: PropTypes.number.isRequired,
    email: PropTypes.string.isRequired,
    coupon: PropTypes.string.isRequired,
    datetime: PropTypes.string.isRequired
}

export default ListTable
